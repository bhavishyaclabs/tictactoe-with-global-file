//
//  ViewController2.swift
//  Tic Tac Toe MultiPlayer
//
//  Created by apple on 04/02/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import Foundation
import UIKit

var nextNumber = 2
class ViewController2 : UIViewController {
    //takes the name of players to next screen...
    @IBAction func playButton(sender: AnyObject) {
        playerOne = playerOneName.text
        playerTwo = playerTwoName.text
    }
    @IBOutlet weak var playerOneName: UITextField!
    @IBOutlet weak var playerTwoName: UITextField!
    @IBOutlet weak var zeroTwo: UIButton!
    @IBOutlet weak var zeroOne: UIButton!
    @IBOutlet weak var crossOne: UIButton!
    @IBOutlet weak var crossTwo: UIButton!
    //choosing the signs of croos or zero...
    @IBAction func crossOnePressed(sender: AnyObject) {
        zeroOne.alpha = 1
        crossOne.alpha = 0
        nextNumber = 2
        crossTwo.alpha = 1
        zeroTwo.alpha = 0
    }
    @IBAction func zeroOnePressed(sender: AnyObject) {
        crossOne.alpha = 1
        zeroOne.alpha = 0
        nextNumber = 1
        zeroTwo.alpha = 1
        crossTwo.alpha = 0
    }
    @IBAction func zeroTwoPressed(sender: AnyObject) {
        crossOne.alpha = 0
        zeroOne.alpha = 1
        nextNumber = 2
        zeroTwo.alpha = 0
        crossTwo.alpha = 1
    }
    @IBAction func crossTwoPressed(sender: AnyObject) {
        zeroOne.alpha = 0
        crossOne.alpha = 1
        nextNumber = 1
        crossTwo.alpha = 0
        zeroTwo.alpha = 1
    }
    //for running into portrait only...
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //for keyboard managing....
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
}
