//
//  ViewController.swift
//  Tic Tac Toe MultiPlayer
//
//  Created by apple on 02/02/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

//var winningGame = 0
class ViewController: UIViewController {
    var goNumber = 0
    var winner = 0
    var ticTacToeFull = 0
    // 0 = empty, 1 = zero , 2 = cross
    var gameState = [0,0,0,0,0,0,0,0,0]
    //win if these combinations have same sign...
    let winningCombinations = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    @IBOutlet weak var buttonOutlet: UIButton!
    @IBOutlet weak var labelDisplayed: UILabel!
    @IBOutlet weak var playAgain: UIButton!
    @IBAction func playAgainPressed(sender: AnyObject) {
        //resets all buttons to default when played again...
        goNumber = nextNumber
        pressSave = 0
        ticTacToeFull = 0
        winner = 0
        labelDisplayed.text = ""
        gameState = [0,0,0,0,0,0,0,0,0]
        for index in 0..<9 {
            var button : UIButton
            button = view.viewWithTag(index) as UIButton
            button.setImage(nil, forState: .Normal)
        }
    }
    @IBAction func buttonPressed(sender: AnyObject) {
        //decides if cross should come or zero...
        if gameState[sender.tag] == 0 && winner == 0 {
            ticTacToeFull++
            var image = UIImage()
            
            if goNumber % 2 == 0 {
                image = UIImage(named: "cross.png")!
                gameState[sender.tag] = 2
            } else {
                image = UIImage(named: "zero.png")!
                gameState[sender.tag] = 1
            }
            //if combination of signs are same and they are not empty...
            for combinations in winningCombinations {
                if (gameState[combinations[0]] == gameState[combinations[1]] && gameState[combinations[1]] == gameState[combinations[2]] && gameState[combinations[0]] != 0) {
                    
                    winner = gameState[combinations[0]]
                }
            }
            //if there is no winner
            if winner != 0 {
                //winningGame = win for player 1, win2 for player 2...
                //if some name is given to both players...
                if playerTwo != "" && playerOne != "" {
                    if winner == 2 && nextNumber == 2 {
                        labelDisplayed.text = " \(playerOne) has won"
                        win++
                    } else if winner == 1 && nextNumber == 1 {
                        labelDisplayed.text = " \(playerOne) has won"
                        win++
                    } else if winner == 2 && nextNumber == 1 {
                        labelDisplayed.text = " \(playerTwo) has won"
                        win2++
                    } else if winner == 1 && nextNumber == 2 {
                        labelDisplayed.text = " \(playerTwo) has won"
                        win2++
                    }
                    println(winner)
                    //If no name is given to any player...
                } else if playerTwo == "" && playerOne == "" {
                    println("entering")
                    if winner == 2 && nextNumber == 2 {
                        labelDisplayed.text = "Player one has won"
                        win++
                        println("entering")
                    } else if winner == 1 && nextNumber == 1 {
                        labelDisplayed.text = "Player one has won"
                        win++
                    }
                    else if winner == 2 && nextNumber == 1 {
                        labelDisplayed.text = "Player two has won"
                        win2++
                    }
                    else if winner == 1 && nextNumber == 2 {
                        labelDisplayed.text = "Player two has won"
                        win2++
                    }
                    //if player 2 has no name and player 1 has some name...
                } else if playerTwo == "" && playerOne != "" {
                    if winner == 2 && nextNumber == 2 {
                        labelDisplayed.text = "\(playerOne) has won"
                        win++
                    } else if winner == 1 && nextNumber == 1 {
                        labelDisplayed.text = "\(playerOne) has won"
                        win++
                    } else if winner == 2 && nextNumber == 1 {
                        labelDisplayed.text = "Player Two has won"
                        win2++
                    } else if winner == 1 && nextNumber == 2 {
                        labelDisplayed.text = "Player Two has won"
                        win2++
                    }
                    //if player 2 has a name and player 1 has no name...
                } else if playerTwo != "" && playerOne == "" {
                    if (winner == 2 && nextNumber == 2) {
                        labelDisplayed.text = "Player One has won"
                        win++
                    } else if winner == 1 && nextNumber == 1 {
                        labelDisplayed.text = "Player One has won"
                        win++
                    } else if winner == 2 && nextNumber == 1 {
                        labelDisplayed.text = "\(playerTwo) has won"
                        win2++
                    } else if winner == 1 && nextNumber == 2 {
                        labelDisplayed.text = "\(playerTwo) has won"
                        win2++
                    }
                }
            }
            println(winner)
            //if there is no winner and board is full then game is a tie...
            if winner == 0 && ticTacToeFull == 9 {
                labelDisplayed.text = "Match is a tie"
                matchTieCount++
            }
            goNumber++
            sender.setImage(image, forState: .Normal)
        }
    }
    //for running into portrait only...
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
    //returns to main board and keeps the sign as default ie cross...
    @IBAction func quitButton(sender: AnyObject) {
        //moves to main screen by resetting all the values...
        goNumber = nextNumber
        nextNumber = 2 //cross
        win = 0
        win2 = 0
        matchTieCount = 0
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        goNumber = nextNumber
    }
}

